PGDMP     %    .                x         	   XsisRedux    12.2    12.2 "    '           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            (           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            )           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            *           1262    26323 	   XsisRedux    DATABASE     �   CREATE DATABASE "XsisRedux" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Indonesian_Indonesia.1252' LC_CTYPE = 'Indonesian_Indonesia.1252';
    DROP DATABASE "XsisRedux";
                postgres    false            �            1259    26365    agama    TABLE     c   CREATE TABLE public.agama (
    agama_id integer NOT NULL,
    nama_agama character varying(30)
);
    DROP TABLE public.agama;
       public         heap    postgres    false            �            1259    26363    agama_agama_id_seq    SEQUENCE     �   CREATE SEQUENCE public.agama_agama_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.agama_agama_id_seq;
       public          postgres    false    203            +           0    0    agama_agama_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.agama_agama_id_seq OWNED BY public.agama.agama_id;
          public          postgres    false    202            �            1259    26377 	   identitas    TABLE     r   CREATE TABLE public.identitas (
    identitas_jenis integer NOT NULL,
    nama_identitas character varying(30)
);
    DROP TABLE public.identitas;
       public         heap    postgres    false            �            1259    26375    identitas_identitas_jenis_seq    SEQUENCE     �   CREATE SEQUENCE public.identitas_identitas_jenis_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.identitas_identitas_jenis_seq;
       public          postgres    false    207            ,           0    0    identitas_identitas_jenis_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.identitas_identitas_jenis_seq OWNED BY public.identitas.identitas_jenis;
          public          postgres    false    206            �            1259    26383    jenjang    TABLE     i   CREATE TABLE public.jenjang (
    jenjang_id integer NOT NULL,
    nama_jenjang character varying(30)
);
    DROP TABLE public.jenjang;
       public         heap    postgres    false            �            1259    26381    jenjang_jenjang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jenjang_jenjang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.jenjang_jenjang_id_seq;
       public          postgres    false    209            -           0    0    jenjang_jenjang_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.jenjang_jenjang_id_seq OWNED BY public.jenjang.jenjang_id;
          public          postgres    false    208            �            1259    26371    kelamin    TABLE     t   CREATE TABLE public.kelamin (
    jeniskelamin_id integer NOT NULL,
    nama_jenis_kelamin character varying(30)
);
    DROP TABLE public.kelamin;
       public         heap    postgres    false            �            1259    26369    kelamin_jeniskelamin_id_seq    SEQUENCE     �   CREATE SEQUENCE public.kelamin_jeniskelamin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.kelamin_jeniskelamin_id_seq;
       public          postgres    false    205            .           0    0    kelamin_jeniskelamin_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.kelamin_jeniskelamin_id_seq OWNED BY public.kelamin.jeniskelamin_id;
          public          postgres    false    204            �            1259    26411    profile    TABLE     �  CREATE TABLE public.profile (
    id integer NOT NULL,
    nama character varying(30) NOT NULL,
    ultah date NOT NULL,
    tempat_lahir character varying(30) NOT NULL,
    id_agama bigint NOT NULL,
    id_jeniskelamin bigint NOT NULL,
    tinggi character varying(30) NOT NULL,
    berat character varying(30) NOT NULL,
    kewarganegaraan character varying(30) NOT NULL,
    jenis_identitas bigint NOT NULL,
    nomor_identitas character varying(30) NOT NULL,
    hobby character varying(50),
    alamat character varying(50),
    rt character varying(30),
    rw character varying(30),
    kode_pos character varying(30),
    kelurahan character varying(30),
    kecamatan character varying(30),
    kota character varying(30),
    id_jenjang bigint,
    nama_sekolah character varying(50),
    tahun_mulai character varying(30),
    tahun_selesai character varying(30),
    jurusan character varying(30),
    is_delete boolean
);
    DROP TABLE public.profile;
       public         heap    postgres    false            �            1259    26409    profile_id_seq    SEQUENCE     �   CREATE SEQUENCE public.profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.profile_id_seq;
       public          postgres    false    211            /           0    0    profile_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.profile_id_seq OWNED BY public.profile.id;
          public          postgres    false    210            �
           2604    26368    agama agama_id    DEFAULT     p   ALTER TABLE ONLY public.agama ALTER COLUMN agama_id SET DEFAULT nextval('public.agama_agama_id_seq'::regclass);
 =   ALTER TABLE public.agama ALTER COLUMN agama_id DROP DEFAULT;
       public          postgres    false    202    203    203            �
           2604    26380    identitas identitas_jenis    DEFAULT     �   ALTER TABLE ONLY public.identitas ALTER COLUMN identitas_jenis SET DEFAULT nextval('public.identitas_identitas_jenis_seq'::regclass);
 H   ALTER TABLE public.identitas ALTER COLUMN identitas_jenis DROP DEFAULT;
       public          postgres    false    207    206    207            �
           2604    26386    jenjang jenjang_id    DEFAULT     x   ALTER TABLE ONLY public.jenjang ALTER COLUMN jenjang_id SET DEFAULT nextval('public.jenjang_jenjang_id_seq'::regclass);
 A   ALTER TABLE public.jenjang ALTER COLUMN jenjang_id DROP DEFAULT;
       public          postgres    false    209    208    209            �
           2604    26374    kelamin jeniskelamin_id    DEFAULT     �   ALTER TABLE ONLY public.kelamin ALTER COLUMN jeniskelamin_id SET DEFAULT nextval('public.kelamin_jeniskelamin_id_seq'::regclass);
 F   ALTER TABLE public.kelamin ALTER COLUMN jeniskelamin_id DROP DEFAULT;
       public          postgres    false    204    205    205            �
           2604    26414 
   profile id    DEFAULT     h   ALTER TABLE ONLY public.profile ALTER COLUMN id SET DEFAULT nextval('public.profile_id_seq'::regclass);
 9   ALTER TABLE public.profile ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    211    211                      0    26365    agama 
   TABLE DATA           5   COPY public.agama (agama_id, nama_agama) FROM stdin;
    public          postgres    false    203   +'                  0    26377 	   identitas 
   TABLE DATA           D   COPY public.identitas (identitas_jenis, nama_identitas) FROM stdin;
    public          postgres    false    207   k'       "          0    26383    jenjang 
   TABLE DATA           ;   COPY public.jenjang (jenjang_id, nama_jenjang) FROM stdin;
    public          postgres    false    209   �'                 0    26371    kelamin 
   TABLE DATA           F   COPY public.kelamin (jeniskelamin_id, nama_jenis_kelamin) FROM stdin;
    public          postgres    false    205   �'       $          0    26411    profile 
   TABLE DATA           $  COPY public.profile (id, nama, ultah, tempat_lahir, id_agama, id_jeniskelamin, tinggi, berat, kewarganegaraan, jenis_identitas, nomor_identitas, hobby, alamat, rt, rw, kode_pos, kelurahan, kecamatan, kota, id_jenjang, nama_sekolah, tahun_mulai, tahun_selesai, jurusan, is_delete) FROM stdin;
    public          postgres    false    211   �'       0           0    0    agama_agama_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.agama_agama_id_seq', 4, true);
          public          postgres    false    202            1           0    0    identitas_identitas_jenis_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.identitas_identitas_jenis_seq', 2, true);
          public          postgres    false    206            2           0    0    jenjang_jenjang_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.jenjang_jenjang_id_seq', 4, true);
          public          postgres    false    208            3           0    0    kelamin_jeniskelamin_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.kelamin_jeniskelamin_id_seq', 2, true);
          public          postgres    false    204            4           0    0    profile_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.profile_id_seq', 3, true);
          public          postgres    false    210               0   x�3��,�I��2��.�,.I��2�t*MI�H�2����K)����� �*
�             x�3��.)�2�������� #qj      "   !   x�3�t1�2�t1�2�6�2�6����� 20�             x�3�,(�L�2�H-J�-(M������ Wd�      $   �   x�eP�n� <���?�%�)ǜ*WjU�q�zYŏRd�-���.ඇ
1;3 �BRH��e�P�S%:�P�҅�D�x��J"��1������J��7d�,�Yf�� 7�,�S
�;礙�ɸo(��3�ʣ���E�Zbywd�/��q��h0��]L:d���{r'�F��|?�+�J	�lt����
'��8� ϯ�&�\9\�:OgOa�������:.^z�-[7�~��sr���(�o4Ii+     