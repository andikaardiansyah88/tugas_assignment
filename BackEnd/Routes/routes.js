const BiodataLogic = require('../Biodata/Biodata_BL.js')
module.exports = exports = function(server){
    server.get('/api/lihatbiodata',BiodataLogic.readBiodataAllHandler)
    server.get('/api/lihatagama',BiodataLogic.readAgamaAllHandler)
    server.get('/api/lihatkelamin',BiodataLogic.readKelaminAllHandler)
    server.get('/api/lihatidentitas',BiodataLogic.readIdentitasAllHandler)
    server.get('/api/lihatjenjang',BiodataLogic.readJenjangAllHandler)
    server.post('/api/tambahbiodata',BiodataLogic.tambahBiodataAllHandler)
    server.put('/api/editbiodata',BiodataLogic.updateBiodataAllHandler)
    server.put('/api/editalamat',BiodataLogic.updateAlamatAllHandler)
    server.put('/api/editjenjang',BiodataLogic.updateJenjangAllHandler)
}