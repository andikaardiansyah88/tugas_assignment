const ResponseHelper = require ('../Helpers/responseHelper')
const dtl = require ('../DataLayer/dt')
const Biodata_BL ={
    readBiodataAllHandler:(req,res,next)=>{
        dtl.readBiodataAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readAgamaAllHandler:(req,res,next)=>{
        dtl.readAgamaAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readKelaminAllHandler:(req,res,next)=>{
        dtl.readKelaminAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readIdentitasAllHandler:(req,res,next)=>{
        dtl.readIdentitasAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readJenjangAllHandler:(req,res,next)=>{
        dtl.readJenjangAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    tambahBiodataAllHandler: (req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.tambahBiodataAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     }, updateBiodataAllHandler: (req,res,next)=>{
        var docs=req.body
        dtl.updateBiodataAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     updateAlamatAllHandler: (req,res,next)=>{
        var docs=req.body
        dtl.updateAlamatAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     updateJenjangAllHandler: (req,res,next)=>{
        var docs=req.body
        dtl.updateJenjangAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     }
}
module.exports= Biodata_BL