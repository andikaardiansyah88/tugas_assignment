const pg = require('pg')
const DatabaseConnection = require('../Config/dbp.config.json')
var DB = new pg.Pool(DatabaseConnection.config)
const bcrypt = require('bcryptjs')
const dt ={
    readBiodataAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select profile.*,agama.nama_agama,identitas.nama_identitas,kelamin.nama_jenis_kelamin,jenjang.nama_jenjang from profile inner join agama on profile.id_agama = agama.agama_id inner join identitas on profile.jenis_identitas = identitas.identitas_jenis inner join kelamin on profile.id_jeniskelamin = kelamin.jeniskelamin_id inner join jenjang on profile.id_jenjang = jenjang.jenjang_id',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = { year: 'numeric', month: '2-digit', day: '2-digit' }
                        let exptoken = (result.rows[i].ultah).toLocaleDateString(undefined, options)

                        result.rows[i].ultah = exptoken

                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readAgamaAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select * from agama',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readKelaminAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select * from kelamin',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readIdentitasAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select * from identitas',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readJenjangAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select * from jenjang',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    tambahBiodataAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into profile (nama,ultah,tempat_lahir,id_agama,id_jeniskelamin,tinggi,berat,kewarganegaraan,jenis_identitas,nomor_identitas,hobby,alamat,rt,rw,kode_pos,kelurahan,kecamatan,kota,id_jenjang,nama_sekolah,tahun_mulai,tahun_selesai,jurusan,is_delete) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,false)',
                values:[docs.nama,docs.ultah,docs.tempat_lahir,docs.id_agama,docs.id_jeniskelamin,docs.tinggi,docs.berat,docs.kewarganegaraan,docs.jenis_identitas,docs.nomor_identitas,docs.hobby,docs.alamat,docs.rt,docs.rw,docs.kode_pos,docs.kelurahan,docs.kecamatan,docs.kota,docs.id_jenjang,docs.nama_sekolah,docs.tahun_mulai,docs.tahun_selesai,docs.jurusan]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    updateBiodataAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update profile set nama=($1),ultah=($2),tempat_lahir=($3),id_agama=($4),id_jeniskelamin=($5),tinggi=($6),berat=($7),kewarganegaraan=($8),jenis_identitas=($9),nomor_identitas=($10),hobby=($11) where id=($12)',
                values:[docs.nama,docs.ultah,docs.tempat_lahir,docs.id_agama,docs.id_jeniskelamin,docs.tinggi,docs.berat,docs.kewarganegaraan,docs.jenis_identitas,docs.nomor_identitas,docs.hobby,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },
    updateAlamatAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update profile set alamat=($1),rt=($2),rw=($3),kode_pos=($4),kelurahan=($5),kecamatan=($6),kota=($7) where id=($8)',
                values:[docs.alamat,docs.rt,docs.rw,docs.kode_pos,docs.kelurahan,docs.kecamatan,docs.kota,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },
    updateJenjangAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update profile set id_jenjang=($1),nama_sekolah=($2),tahun_mulai=($3),tahun_selesai=($4),jurusan=($5) where id=($6)',
                values:[docs.id_jenjang,docs.nama_sekolah,docs.tahun_mulai,docs.tahun_selesai,docs.jurusan,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    }
}
module.exports = dt