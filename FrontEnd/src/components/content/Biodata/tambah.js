import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class TambahList extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
        this.state={
            biodata:{
                nama:'',ultah:'',tempat_lahir:'',id_agama:'',id_jeniskelamin:'',tinggi:'',berat:'',kewarganegaraan:'',jenis_identitas:'',nomor_identitas:'',hobby:'',alamat:'',rt:'',rw:'',kode_pos:'',kelurahan:'',kecamatan:'',kota:'',id_jenjang:'',nama_sekolah:'',tahun_mulai:'',tahun_selesai:'',jurusan:''
            },Agama:[],Kelamin:[],Identitas:[],Jenjang:[]
             }
        this.changeHandler = this.changeHandler.bind(this)
        this.close = this.close.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
    }
    changeHandler(e){
        let tmp = this.state.biodata
        tmp[e.target.name]=e.target.value
        this.setState({
            formdata:tmp
        })
    }
    Tanggal(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
        var b = new Date().getHours();
        var bb = new Date().getMinutes();
        var bbb = new Date().getSeconds();
        var c = a+'-'+aa+'-'+aaa+" "+b+":"+bb+":"+bbb
        this.setState({
            Tanggal:c
        })
    }
    componentDidMount(){
        this.Tanggal()
        this.getListAgama()
        this.getListJenisKelamin()
        this.getListIdentitas()
        this.getListJenjang()

    }
    getListAgama(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.AGAMA,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Agama: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenisKelamin(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.KELAMIN,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Kelamin: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListIdentitas(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.IDENTITAS,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Identitas: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenjang(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.JENJANG,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Jenjang: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    close(){
        this.setState({
            formdata:{},CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        })
        this.props.closeModalhandler()
    }
    submitHandler(){

        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHBIODATA,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.biodata
        }



        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
    this.props.closeModalhandler()

}
    render(){
        return(
            <Modal isOpen={this.props.tambah} className={this.props.className} size="lg">
                
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}>Biodata Form</ModalHeader>
                <ModalBody>
                <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Nama Lengkap *</Col><Col width="200px">Tanggal Lahir</Col> <Col width="200px">Tempat Lahir</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="nama"
                    value={this.state.biodata.nama}
                    onChange={this.changeHandler}
                    reqiured placeholder="Nama Lengkap"/>
                 <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.CError}</Label></Col><Col width="200px">
                     <Input type="date" class="form-control"
                    name="ultah"
                    value={this.state.biodata.ultah}
                    onChange={this.changeHandler}
                   />
                   </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tempat_lahir"
                    value={this.state.biodata.tempat_lahir}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tempat Lahir"/>
                     </Col></Row>
                         <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Agama</Col><Col width="200px">Jenis Kelamin</Col><Col width="200px">Tinggi Badan</Col></Row>
                         <Row ><Col width="200px">
                     <select 
                      className="form-control" 
                      name="id_agama" 
                      onChange={this.changeHandler}
                      value={this.state.biodata.id_agama}>
                         <option value=''>Pilih</option>
                         {this.state.Agama.map((row,x)=>
                                <option value={row.agama_id}>{row.nama_agama}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                     <select 
                      className="form-control" 
                      name="id_jeniskelamin" 
                      onChange={this.changeHandler}
                      value={this.state.biodata.id_jeniskelamin}>
                         <option value=''>Pilih</option>
                         {this.state.Kelamin.map((row,x)=>
                                <option value={row.jeniskelamin_id}>{row.nama_jenis_kelamin}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tinggi"
                    value={this.state.biodata.tinggi}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tinggi Badan"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Berat</Col><Col width="200px">Kewarganegaraan</Col><Col width="200px">Jenis Identitas</Col><Col width="200px">Nomor Identitas</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="berat"
                    value={this.state.biodata.berat}
                    onChange={this.changeHandler}
                    reqiured placeholder="Berat Badan"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kewarganegaraan"
                    value={this.state.biodata.kewarganegaraan}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kewarganegaraan"/>
                     </Col><Col width="200px">
                     <select 
                      className="form-control" 
                      name="jenis_identitas" 
                      onChange={this.changeHandler}
                      value={this.state.biodata.jenis_identitas}>
                         <option value=''>Pilih</option>
                         {this.state.Identitas.map((row,x)=>
                                <option value={row.identitas_jenis}>{row.nama_identitas}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="nomor_identitas"
                    value={this.state.biodata.nomor_identitas}
                    onChange={this.changeHandler}
                    reqiured placeholder="NomorIdentitas"/>
                     </Col></Row>
                    <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Hobby</Col><Col width="200px">Alamat</Col><Col width="200px">RT</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="hobby"
                    value={this.state.biodata.hobby}
                    onChange={this.changeHandler}
                    reqiured placeholder="Hobby"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="alamat"
                    value={this.state.biodata.alamat}
                    onChange={this.changeHandler}
                    reqiured placeholder="Alamat"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="rt"
                    value={this.state.biodata.rt}
                    onChange={this.changeHandler}
                    reqiured placeholder="RT"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">RW</Col><Col width="200px">Kode Pos</Col><Col width="200px">Kelurahan</Col><Col width="200px">Kecamatan</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="rw"
                    value={this.state.biodata.rw}
                    onChange={this.changeHandler}
                    reqiured placeholder="RW"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kode_pos"
                    value={this.state.biodata.kode_pos}
                    onChange={this.changeHandler}
                    reqiured placeholder="kode pos"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kelurahan"
                    value={this.state.biodata.kelurahan}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kelurahan"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kecamatan"
                    value={this.state.biodata.kecamatan}
                    onChange={this.changeHandler}
                    reqiured placeholder="kecamatan"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Kota</Col><Col width="200px">Jenjang Pendidikan</Col><Col width="200px">Nama Sekolah</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kota"
                    value={this.state.biodata.kota}
                    onChange={this.changeHandler}
                    reqiured placeholder="kota"/>
                     </Col><Col width="200px">
                     <select 
                      className="form-control" 
                      name="id_jenjang" 
                      onChange={this.changeHandler}
                      value={this.state.biodata.id_jenjang}>
                         <option value=''>Pilih</option>
                         {this.state.Jenjang.map((row,x)=>
                                <option value={row.jenjang_id}>{row.nama_jenjang}</option>)
                            }   
                 </select>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="nama_sekolah"
                    value={this.state.biodata.nama_sekolah}
                    onChange={this.changeHandler}
                    reqiured placeholder="nama sekolah"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Tahun Mulai</Col><Col width="200px">Tahun Selesai</Col><Col width="200px">Jurusan</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tahun_mulai"
                    value={this.state.biodata.tahun_mulai}
                    onChange={this.changeHandler}
                    reqiured placeholder="tahun mulai"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tahun_selesai"
                    value={this.state.biodata.tahun_selesai}
                    onChange={this.changeHandler}
                    reqiured placeholder="tahun selesai"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="jurusan"
                    value={this.state.biodata.jurusan}
                    onChange={this.changeHandler}
                    reqiured placeholder="jurusan"/>
                     </Col></Row>
                          </ModalBody>
                <ModalFooter>
                    <Button color="warning"onClick={this.close}>Close</Button>
                    <Button color="primary" onClick={this.submitHandler}>Save</Button></ModalFooter>
            </Modal>
        )
    }
} export default TambahList