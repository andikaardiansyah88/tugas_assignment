import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Collapse,Row,Col,Label,Input,Form,FormGroup,ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'


class Detail extends React.Component{
    constructor(props){
        super(props)
        this.state={
            information:false,PiCol:false,PD:false,PT:false,MT:false
        }
    this.dropdownInformationO = this.dropdownInformationO.bind(this)
    this.dropdownInformationC = this.dropdownInformationC.bind(this)
}
PICol(){
this.setState({
  PiCol:!this.state.PiCol,
  PD:false,
  PT:false,
  MT:false
})
}
PDonClick(){
  this.setState({
    PD:!this.state.PD,
    PiCol:false,
    PT:false,
    MT:false
  })
  }
  PTonClick(){
    this.setState({
      PT:!this.state.PT,
      PiCol:false,
      PD:false,
      MT:false
    })
    }
   

dropdownInformationO(){
    this.setState({
        information:true
    })
}
dropdownInformationC(){
    this.setState({
        information:false
    })
}

    render(){
        return(
            <Modal style={{color:'#000066'}}isOpen={this.props.detail} className={this.props.className} size='lg'>
                  <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}><Container><Row><Col size="lg" xs="6" style={{columnWidth:'700px'}}>Detail</Col><Col size="sm"><Button style={{color:'#ffffff',float:'right'}}close onClick={this.props.closeModalhandler} class="rounded float-right"/></Col></Row></Container></ModalHeader>
                  <ModalBody>
                   
                  <ul class="nav nav-pills nav-sidebar flex-column"  role="menu" data-toggle="collapse" size="lg"style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                  <li class="nav-item menu-close">
                    <a href="#" class="nav-link " onClick={this.PICol.bind(this)} onC style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Biodata Information
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PiCol} hide style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                         <Row><Col width="200px">Nama Lengkap </Col><Col width="200px">Tanggal Lahir</Col><Col width="200px">Tempat Lahir</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.nama}</Col><Col width="200px">{this.props.biodata.ultah}</Col><Col width="200px">{this.props.biodata.tempat_lahir}</Col></Row>
                         <Row><Col width="200px">Agama </Col><Col width="200px">Jenis Kelamin</Col><Col width="200px">Tinggi Badan</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.nama_agama}</Col><Col width="200px">{this.props.biodata.nama_jenis_kelamin}</Col><Col width="200px">{this.props.biodata.tinggi}</Col></Row>
                         <Row><Col width="200px">Berat </Col><Col width="200px">Kewarganegaraan</Col><Col width="200px">Jenis Identitas</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.berat}</Col><Col width="200px">{this.props.biodata.kewarganegaraan}</Col><Col width="200px">{this.props.biodata.nama_identitas}</Col></Row>
                         <Row><Col width="200px">Nomor Identitas </Col><Col width="200px">Hobby</Col><Col width="200px">-</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.nomor_identitas}</Col><Col width="200px">{this.props.biodata.hobby}</Col><Col width="200px">-</Col></Row>
                    </Collapse>    
                  </li>
                  <li class="nav-item menu-close">
                    <a href="#" class="nav-link " onClick={this.PDonClick.bind(this)} onC style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Alamat Information
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PD} hide style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                         <Row><Col width="200px">Alamat Saat Ini </Col><Col width="200px">RT</Col><Col width="200px">RW</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.alamat}</Col><Col width="200px">{this.props.biodata.rt}</Col><Col width="200px">{this.props.biodata.rw}</Col></Row>
                         <Row><Col width="200px">Kode Pos </Col><Col width="200px">kelurahan</Col><Col width="200px">Kecamatan</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.kode_pos}</Col><Col width="200px">{this.props.biodata.kelurahan}</Col><Col width="200px">{this.props.biodata.kecamatan}</Col></Row>
                         <Row><Col width="200px">kota </Col><Col width="200px">-</Col><Col width="200px">-</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.kota}</Col><Col width="200px">-</Col><Col width="200px">-</Col></Row>
                    
                    </Collapse>
                    
                  </li>
                  <li class="nav-item menu-close">
                    <a href="#" class="nav-link " onClick={this.PTonClick.bind(this)} onC style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Pendidikan Information
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PT} hide style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                         <Row><Col width="200px">jenjang Pendidikan</Col><Col width="200px">nama Sekolah</Col><Col width="200px">-</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.nama_jenjang}</Col><Col width="200px">{this.props.biodata.nama_sekolah}</Col><Col width="200px">-</Col></Row>
                         <Row><Col width="200px">Tahun Mulai </Col><Col width="200px">Tahun Selesai</Col><Col width="200px">Jurusan</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.biodata.tahun_mulai}</Col><Col width="200px">{this.props.biodata.tahun_selesai}</Col><Col width="200px">{this.props.biodata.jurusan}</Col></Row>
                        
                    
                    </Collapse>
                    
                  </li>
                </ul>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="warning"onClick={this.props.closeModalhandler}>Close</Button>
                  </ModalFooter>
            </Modal>
        )
    }
}export default Detail