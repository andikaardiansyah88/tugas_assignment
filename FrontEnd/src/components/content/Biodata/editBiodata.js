import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class EditBiodata extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
        this.state={
            biodata:{
                nama:'',ultah:'',tempat_lahir:'',id_agama:'',id_jeniskelamin:'',tinggi:'',berat:'',kewarganegaraan:'',jenis_identitas:'',nomor_identitas:'',hobby:'',alamat:'',rt:'',rw:'',kode_pos:'',kelurahan:'',kecamatan:'',kota:'',id_jenjang:'',nama_sekolah:'',tahun_mulai:'',tahun_selesai:'',jurusan:''
            },Agama:[],Kelamin:[],Identitas:[],Jenjang:[]
             }
        this.changeHandler = this.changeHandler.bind(this)
        this.close = this.close.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
    }
    changeHandler(e){
        let tmp = this.props.biodata
        tmp[e.target.name]=e.target.value
        this.setState({
            biodata:tmp
        })
    }
   
    componentDidMount(){
        this.getListAgama()
        this.getListJenisKelamin()
        this.getListIdentitas()
        this.getListJenjang()

    }
    getListAgama(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.AGAMA,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Agama: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenisKelamin(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.KELAMIN,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Kelamin: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListIdentitas(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.IDENTITAS,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Identitas: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenjang(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.JENJANG,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Jenjang: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    close(){
        this.setState({
            formdata:{},CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        })
        this.props.closeModalhandler()
    }
    submitHandler(){

        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.EDITBIODATA,
            method :"put",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.props.biodata
        }



        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
    this.props.closeHandler()

}
    render(){
        return(
            <Modal isOpen={this.props.view} className={this.props.className} size="lg">
                
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}>Biodata Form</ModalHeader>
                <ModalBody>
                <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Nama Lengkap *</Col><Col width="200px">Tanggal Lahir</Col> <Col width="200px">Tempat Lahir</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="nama"
                    value={this.props.biodata.nama}
                    onChange={this.changeHandler}
                    reqiured placeholder="Nama Lengkap"/></Col><Col>
                     <Input type="date" class="form-control"
                    name="ultah"
                    value={this.props.biodata.ultah}
                    onChange={this.changeHandler}
                   />
                   </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tempat_lahir"
                    value={this.props.biodata.tempat_lahir}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tempat Lahir"/>
                     </Col></Row>
                         <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Agama</Col><Col width="200px">Jenis Kelamin</Col><Col width="200px">Tinggi Badan</Col></Row>
                         <Row ><Col width="200px">
                     <select 
                      className="form-control" 
                      name="id_agama" 
                      onChange={this.changeHandler}
                      value={this.props.biodata.id_agama}>
                         <option value=''>Pilih</option>
                         {this.state.Agama.map((row,x)=>
                                <option value={row.agama_id}>{row.nama_agama}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                     <select 
                      className="form-control" 
                      name="id_jeniskelamin" 
                      onChange={this.changeHandler}
                      value={this.props.biodata.id_jeniskelamin}>
                         <option value=''>Pilih</option>
                         {this.state.Kelamin.map((row,x)=>
                                <option value={row.jeniskelamin_id}>{row.nama_jenis_kelamin}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="tinggi"
                    value={this.props.biodata.tinggi}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tinggi Badan"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Berat</Col><Col width="200px">Kewarganegaraan</Col><Col width="200px">Jenis Identitas</Col><Col width="200px">Nomor Identitas</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="berat"
                    value={this.props.biodata.berat}
                    onChange={this.changeHandler}
                    reqiured placeholder="Berat Badan"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kewarganegaraan"
                    value={this.props.biodata.kewarganegaraan}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kewarganegaraan"/>
                     </Col><Col width="200px">
                     <select 
                      className="form-control" 
                      name="jenis_identitas" 
                      onChange={this.changeHandler}
                      value={this.props.biodata.jenis_identitas}>
                         <option value=''>Pilih</option>
                         {this.state.Identitas.map((row,x)=>
                                <option value={row.identitas_jenis}>{row.nama_identitas}</option>)
                            }   
                 </select>
                    </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="nomor_identitas"
                    value={this.props.biodata.nomor_identitas}
                    onChange={this.changeHandler}
                    reqiured placeholder="NomorIdentitas"/>
                     </Col></Row>
                    <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Hobby</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="hobby"
                    value={this.props.biodata.hobby}
                    onChange={this.changeHandler}
                    />
                     </Col></Row>
                          </ModalBody>
                <ModalFooter>
                    <Button color="warning"onClick={this.props.closeHandler}>Close</Button>
                    <Button color="primary" onClick={this.submitHandler}>Save</Button></ModalFooter>
            </Modal>
        )
    }
} export default EditBiodata