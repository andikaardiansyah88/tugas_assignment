import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class EditAlamat extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
        this.state={
            biodata:{
                nama:'',ultah:'',tempat_lahir:'',id_agama:'',id_jeniskelamin:'',tinggi:'',berat:'',kewarganegaraan:'',jenis_identitas:'',nomor_identitas:'',hobby:'',alamat:'',rt:'',rw:'',kode_pos:'',kelurahan:'',kecamatan:'',kota:'',id_jenjang:'',nama_sekolah:'',tahun_mulai:'',tahun_selesai:'',jurusan:''
            },Agama:[],Kelamin:[],Identitas:[],Jenjang:[]
             }
        this.changeHandler = this.changeHandler.bind(this)
        this.close = this.close.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
    }
    changeHandler(e){
        let tmp = this.props.biodata
        tmp[e.target.name]=e.target.value
        this.setState({
            biodata:tmp
        })
    }
   
    componentDidMount(){
        this.getListAgama()
        this.getListJenisKelamin()
        this.getListIdentitas()
        this.getListJenjang()

    }
    getListAgama(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.AGAMA,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Agama: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenisKelamin(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.KELAMIN,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Kelamin: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListIdentitas(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.IDENTITAS,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Identitas: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListJenjang(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.JENJANG,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Jenjang: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    close(){
        this.setState({
            formdata:{},CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        })
        this.props.closeModalhandler()
    }
    submitHandler(){

        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.EDITALAMAT,
            method :"put",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.props.biodata
        }



        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
    this.props.closeHandler()

}
    render(){
        return(
            <Modal isOpen={this.props.view} className={this.props.className} size="lg">
                
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}>Biodata Form</ModalHeader>
                <ModalBody>
                <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Alamat</Col><Col width="200px">RT</Col><Col width="200px">RW</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="alamat"
                    value={this.props.biodata.alamat}
                    onChange={this.changeHandler}
                    reqiured placeholder="Alamat"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="rt"
                    value={this.props.biodata.rt}
                    onChange={this.changeHandler}
                    reqiured placeholder="RT"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="rw"
                    value={this.props.biodata.rw}
                    onChange={this.changeHandler}
                    reqiured placeholder="RW"/>
                     </Col></Row>
                     <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Kode Pos</Col><Col width="200px">Kelurahan</Col><Col width="200px">Kecamatan</Col><Col width="200px">Kota</Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kode_pos"
                    value={this.props.biodata.kode_pos}
                    onChange={this.changeHandler}
                    reqiured placeholder="kode pos"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kelurahan"
                    value={this.props.biodata.kelurahan}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kelurahan"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kecamatan"
                    value={this.props.biodata.kecamatan}
                    onChange={this.changeHandler}
                    reqiured placeholder="kecamatan"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="kota"
                    value={this.props.biodata.kota}
                    onChange={this.changeHandler}
                    reqiured placeholder="kota"/>
                     </Col></Row>
                          </ModalBody>
                <ModalFooter>
                    <Button color="warning"onClick={this.props.closeHandler}>Close</Button>
                    <Button color="primary" onClick={this.submitHandler}>Save</Button></ModalFooter>
            </Modal>
        )
    }
} export default EditAlamat