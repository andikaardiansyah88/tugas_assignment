import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import Detail from './detail'
import ViewBiodata2 from './viewbiodata2'
import Alamat from './alamat'
import Pendidikan from'./pendidikan'
class viewBiodata extends React.Component{
  constructor(props){
    super(props)
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))

    this.state={
        formdata:{
            fullname:'',
            fullname2:''
        },
        currentBiodata:{},
        editBiodata:false,
        biodata:false,
        profile:false,
        alamat:false,
        pendidikan:false,
        viewBiodata : false
    }
this.BiodataButton = this.BiodataButton.bind(this)
this.ProfileButton = this.ProfileButton.bind(this)
this.PendidikanButton = this.PendidikanButton.bind(this)
this.AlamatButton = this.AlamatButton.bind(this)
this.closeModalhandler = this.closeModalhandler.bind(this)


  }

  closeHandler(){
    this.setState({
        editBiodata : false
        
    })
    
}
PendidikanButton(){
  this.setState({
    biodata : false,
    profile:false,
    alamat:false,
    pendidikan:true
  })
}
AlamatButton(){
  this.setState({
    biodata : false,
    profile:false,
    alamat:true,
    pendidikan:false
  })
}
BiodataButton(){
  this.setState({
    biodata : true,
    profile:false,
    alamat:false,
    pendidikan:false
  })
}
ProfileButton(){
  this.setState({
    biodata : false,
    profile:true,
    alamat:false,
    pendidikan:false
  })
}
closeModalhandler(){
  this.setState({
    profile:false
  })
}
popupHandler(){
  this.setState({
      viewBiodata : false,
      editBiodata:true   
  })
  this.getListBiodata()
}
    render(){
        return(
            <Modal style={{color:'#000066'}}isOpen={this.props.view} className={this.props.className} size='lg'>
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}><Container><Row><Col size="lg" xs="6" style={{columnWidth:'700px'}}>Detail Pelamar</Col><Col size="sm"><Button style={{color:'#ffffff',float:'right'}}close onClick={this.props.closeModalHandler} class="rounded float-right"/></Col></Row></Container></ModalHeader>
                <ModalBody>
                    <Container>
                        <Row>
                            <Col xs="3">
                            <div>
      <Button outline color="secondary" size="sm" block onClick={this.ProfileButton}>Profile</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block onClick={this.BiodataButton}>Biodata</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block onClick={this.AlamatButton} >Alamat</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block onClick={this.PendidikanButton}>Pendidikan</Button>{' '}</div>

    </Col>
    <Col size="lg">
    <Collapse isOpen={this.state.profile}>
          <Detail
          detail = {this.state.profile}
          biodata = {this.props.biodata}
          closeModalhandler = {this.closeModalhandler}/>
      </Collapse>
      <Collapse isOpen={this.state.biodata}>
        <ViewBiodata2
        view = {this.state.biodata}
        biodata = {this.props.biodata}/>
      </Collapse>
      <Collapse isOpen={this.state.alamat}>
        <Alamat
        view = {this.state.alamat}
        biodata = {this.props.biodata}/>
      </Collapse>
      <Collapse isOpen={this.state.pendidikan}>
        <Pendidikan
        view = {this.state.pendidikan}
        biodata = {this.props.biodata}/>
      </Collapse>
    </Col>
    </Row>
    </Container>
                </ModalBody>
                <ModalFooter>
                </ModalFooter>
            </Modal>
        )
    }
}
export default viewBiodata