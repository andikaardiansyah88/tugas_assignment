import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText,Label} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import {Link} from 'react-router-dom'
import EditBiodata from './editBiodata'
class ViewBiodata2 extends React.Component{
    constructor(props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
    
        this.state={
            view:[],view2:[],
            formdata:{},yosdata:{},
            currentBiodata:{},Religion:[],Marital:[],Identity:[],
            tahun:'',
            editBiodata:false,
            biodata:false,
            profile:false,
            viewBiodata:false
        }
        this.editModalHandler1 = this.editModalHandler1.bind(this)
        this.popup = this.popup.bind(this)
        this.closeHandler = this.closeHandler.bind(this)
       this.openHandler = this.openHandler.bind(this)

    }
    viewModalHandler(kodebiodata){
        let tmp = {}
        this.state.view2.map((row)=>{
            if(kodebiodata == row.idbener){
                tmp = row
            }
        })
        this.setState({
            currentBiodata : tmp,
            editBiodata:true   
        })
        this.getListView2()
    }
    closeHandler(){
        this.setState({
            editBiodata:false
        })
  
    }
    openHandler(){
      this.setState({
        editBiodata:true
      })
    }

    editModalHandler1(kodebiodata){
        this.state.view.map((row)=>{
            if(kodebiodata == row.id){
                this.setState({
                    currentBiodata : row,
                    editBiodata:true
                }) 
            }
        })
    }
    popup(){
        this.setState({
            editBiodata:true   
        })
    }
  

    render(){
        return(
            <Card body outline color="secondary" isOpen={this.props.view} className={this.props.className} size="lg" >
            <CardTitle style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}> <table id="mytable" class="table table-bordered table-striped">
                 <thead>
                     <tr>
                         </tr></thead> 
                         <tbody>
                           <td>Biodata</td>
                           <td><button class="fas fa-edit" onClick={this.openHandler} style={{backgroundColor:'#000066',color:'#ffffff',borderRadius:'40px',fontSize:'20px'}}></button></td>
                            </tbody>  
                </table></CardTitle> 
        <CardText> <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Nama Lengkap</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.nama}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tanggal Lahir</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.ultah}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tempat Lahir</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.tempat_lahir}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Agama</Col>
        <Col xs="auto">:</Col>
                            <Col xs="auto" >{ this.props.biodata.nama_agama}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Jenis Kelamin</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.nama_jenis_kelamin}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tinggi Badan(Cm)</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.tinggi}(Cm)</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}> 
        <Col xs="6">Berat Badan (Kg)</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.berat}(Kg)</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Kewarganegaraan</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.kewarganegaraan}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Jenis & Nomor Identitas</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.nama_identitas} - { this.props.biodata.nomor_identitas}</Col>
      </Row>
      <Row >
        <Col xs="6">Hobby</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.hobby}</Col>
      </Row></CardText>
      <EditBiodata biodata = {this.props.biodata}
             closeHandler = {this.closeHandler}
             view = {this.state.editBiodata}/>
      </Card>
        )
    }
}
export default ViewBiodata2