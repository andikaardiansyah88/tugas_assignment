import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText,Label} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import {Link} from 'react-router-dom'
import EditPendidikan from './editpendidikan'
class Pendidikan extends React.Component{
    constructor(props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
    
        this.state={
            view:[],view2:[],
            formdata:{},yosdata:{},
            currentBiodata:{},Religion:[],Marital:[],Identity:[],
            tahun:'',
            editBiodata:false,
            biodata:false,
            profile:false,
            viewBiodata:false
        }
        this.editModalHandler1 = this.editModalHandler1.bind(this)
        this.popup = this.popup.bind(this)
        this.closeHandler = this.closeHandler.bind(this)
        this.openHandler = this.openHandler.bind(this)

    }
   
    closeHandler(){
        this.setState({
            editBiodata:false,
        
        })

  
    }
    openHandler(){
        this.setState({
            editBiodata:true,
        })
  
    }
    editModalHandler1(kodebiodata){
        this.state.view.map((row)=>{
            if(kodebiodata == row.id){
                this.setState({
                    currentBiodata : row,
                    editBiodata:true
                }) 
            }
        })
    }
    popup(){
        this.setState({
            editBiodata:true   
        })
    }
  

    render(){
        return(
            <Card body outline color="secondary" isOpen={this.props.view} className={this.props.className} size="lg" >
            <CardTitle style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}> <table id="mytable" class="table table-bordered table-striped">
                 <thead>
                     <tr>
                         </tr></thead> 
                         <tbody>
                           <td>Pendidikan</td>
                           <td><button onClick={this.openHandler} class="fas fa-edit" style={{backgroundColor:'#000066',color:'#ffffff',borderRadius:'40px',fontSize:'20px'}}></button></td>
                            </tbody>  
                </table></CardTitle> 
        <CardText> <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Jenjang Pendidikan </Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.nama_jenjang}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">nama Sekolah</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.nama_sekolah}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tahun Mulai</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.tahun_mulai}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tahun Selesai</Col>
        <Col xs="auto">:</Col>
                            <Col xs="auto" >{ this.props.biodata.tahun_selesai}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">jurusan</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.props.biodata.jurusan}</Col>
      </Row>
      </CardText>
      <EditPendidikan biodata = {this.props.biodata}
             closeHandler = {this.closeHandler}
             view = {this.state.editBiodata}/>
      </Card>
        )
    }
}
export default Pendidikan