import axios from 'axios'
import apiconfig from '../configs/api.configs.json'


const API = {

    Bio :async (id,
        Nama ,
	ultah ,
	tempatlahir ,
	id_agama ,
	id_jeniskelamin ,
	Tinggi ,
	Berat ,
	Kewarganegaraan ,
	jenis_identitas ,
	nomor_identitas ,
	Hobby ,
	alamat ,
	rt ,
	rw ,
	kode_pos ,
	kelurahan ,
	kecamatan ,
	kota ,
	Pendidikan_terakhir ,
	Nama_sekolah ,
	Tahun_mulai ,
	tahun_selesai ,
	Jurusan ,
	id_jenjang
        )=>{
        let token=localStorage.getItem(apiconfig.LS.TOKEN)
        let option={
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.BIODATA,
            method: "GET",
            headers: {
                "Authorization": token
            },
            data:{
                id : id,
	Nama : Nama ,
	ultah : ultah ,
	tempatlahir : tempatlahir ,
	id_agama : id_agama ,
	id_jeniskelamin : id_jeniskelamin ,
	Tinggi : Tinggi ,
	Berat : Berat ,
	Kewarganegaraan : Kewarganegaraan ,
	jenis_identitas : jenis_identitas ,
	nomor_identitas : nomor_identitas ,
	Hobby : Hobby ,
	alamat : alamat ,
	rt : rt ,
	rw : rw ,
	kode_pos : kode_pos ,
	kelurahan : kelurahan ,
	kecamatan : kecamatan ,
	kota : kota ,
	Pendidikan_terakhir : Pendidikan_terakhir ,
	Nama_sekolah : Nama_sekolah ,
	Tahun_mulai : Tahun_mulai ,
	tahun_selesai : tahun_selesai ,
	Jurusan : Jurusan ,
	id_jenjang : 	id_jenjang
                
            }
        }
        try {
            let result = await axios(option)
            return result.data
        } catch(error){
            return error.response.data
        }
    }

}
export default API